<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('add_user', [UserController::class, 'createUser']);
Route::get('update_user', [UserController::class, 'updateUser']);
Route::get('delete_user/{id}', [UserController::class, 'deletedUser']);
Route::get('get_user', [UserController::class, 'getUser']);
Route::get('get_user_detail/{id}', [UserController::class, 'getUserDetail']);