import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ServiceService } from 'src/app/service.service';
@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent{
  userId :number;
  constructor(private serviceService:ServiceService, public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.userId=data.id
     }

  ngOnInit(): void {
  }
  confirmDelete(){
    console.log(';sdfsd');
    this.serviceService.httpClient.get(this.serviceService.endPoint+"/delete_user/"+this.userId,{ headers: this.serviceService.httpHeaders }).subscribe((response:any) => {
      if(response){
        // console.log(response)
        this.dialogRef.close({
          userMessage: response.message,
        });
      }
    });
  }
  cancelConfirm(){
    this.dialogRef.close();
  }
}
