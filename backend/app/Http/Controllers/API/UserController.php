<?php
namespace App\Http\Controllers\API;
use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ApiResponse;
use App\Models\User;
use Illuminate\Validation\Rule;

class UserController extends Controller {

	public function createUser(Request $request){
		$data = $request->all();
		$validator = Validator::make($request->all(), [
	        'name' => 'required',
	        'surname' => 'required',
	        'email' => 'required|unique:users', 
        ]);
		$user = array(
			'name' => $data['name'],
			'surname' => $data['surname'],
			'email' => $data['email'],
		);
		if($data['id']==0){
		$insert = User::create($user);
		  return ApiResponse::success('User created successfully.');
	    }else{
         $update = User::where('id',$data['id'])->update($user);
		 return ApiResponse::success('User updated successfully.');
	    }
		
	}
	//http://127.0.0.1:8000/api/add_user


	public function updateUser(Request $request){
		$data = $request->all();
		$validator = Validator::make($request->all(), [
	        'name' => 'required',
	        'surname' => 'required',
	        'email' => 'required', 
        ]);
		$user = array(
			'name' => $data['name'],
			'surname' => $data['surname'],
			'email' => $data['email'],
		);
		$update = User::where('id',$data['id'])->update($user);
		return ApiResponse::success('User updated successfully.');
	}
	//http://127.0.0.1:8000/api/update_user

	public function deletedUser($id){
		$update = User::where('id',$id)->delete();
		return ApiResponse::success('User deleted successfully.');
	}
	//http://127.0.0.1:8000/api/delete_user

	public function getUser(Request $request){
		$users = User::get();
		return ApiResponse::success('successfull',$users);
	}

	public function getUserDetail($id){
		$users = User::where('id', $id)->first();
		return ApiResponse::success('successfull',$users);
	}

}