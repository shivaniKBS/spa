import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AdduserformComponent } from './Modal/adduserform/adduserform.component';
import { ServiceService } from './service.service';
import { ConfirmationComponent } from './Modal/confirmation/confirmation.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'spa';
  data:any;
  tableData:any;
  displayedColumns: string[] = ['name', 'surname', 'email'];
 
  constructor(private serviceService: ServiceService,public dialog: MatDialog) { 
    this.getList();
  }
  addUser(id:number): void {
    const dialogRef = this.dialog.open(AdduserformComponent, {
      width: '250px',
      data: { userName: "", userSurname: "" ,userEmail:"",userId:id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.data = result;
      this.getList();
    });
  }
 editUser(id:number){
  if(id!=0){
    this.serviceService.httpClient.get(this.serviceService.endPoint+"/get_user_detail/"+id,{ headers: this.serviceService.httpHeaders }).subscribe((response:any) => {
  if(response){
    console.log(response)
    const dialogRef = this.dialog.open(AdduserformComponent, {
      width: '250px',
      data: { "userName": response.data.name, "userSurname":response.data['surname']  ,"userEmail":response['data'].email,"userId":response['data']['id']}
    });
    
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.data = result;
      this.getList();
      
    });
  }
 }); 
}
 }
  getList(){
       this.serviceService.httpClient.get(this.serviceService.endPoint+"/get_user",{ headers: this.serviceService.httpHeaders }).subscribe((response:any) => {
    if(response){
      this.tableData = response.data;
      // console.log(response)
     
    }
   });
  }
  deleteUser(id:number){
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      width: '250px',
      data: { "id": id}
    });
    
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.data = result;
      this.getList();
      // this.food_from_modal = result.food;
    });
  }
}
