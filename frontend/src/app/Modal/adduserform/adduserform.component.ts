import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ServiceService } from 'src/app/service.service';
@Component({
  selector: 'app-adduserform',
  templateUrl: './adduserform.component.html',
  styleUrls: ['./adduserform.component.css']
})
export class AdduserformComponent{
  userName:any;
  userSurname:any;
  userEmail:any;
  userId:number;
  labelButton:any;
  eData:any;
  constructor(private serviceService:ServiceService, public dialogRef: MatDialogRef<AdduserformComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.userName=data.userName;
      this.userSurname=data.userSurname;
      this.userEmail=data.userEmail;
      this.userId=data.userId;
      this.labelButton = "Create";
      if(this.userId!=0){
        this.labelButton = "Update";
      }
    }
  ngOnInit(): void {
  }
  submitUser(): void {
    this.eData={
      name: this.userName,
      surname: this.userSurname,
      email: this.userEmail,
      id : this.userId
    }
    this.serviceService.httpClient.post(this.serviceService.endPoint+"/add_user",this.eData,{ headers: this.serviceService.httpHeaders }).subscribe((response:any) => {
      if(response){
        // console.log(response)
        this.dialogRef.close({
          userMessage: response.message,
            });
      }
    });
  }
}
